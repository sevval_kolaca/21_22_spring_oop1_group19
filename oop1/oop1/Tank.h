﻿#include <iostream>
#include <fstream>

#define SIZE 10

using namespace std;

class Tank {
public:

	bool isTank = 0, isValve; //tank var mý yok mu
	int Tank[SIZE], Valve[SIZE];
	int will_delete_id;
	int tank_id;
	double fuel_capacity = 100.0;
	int engine_tank = 55;

	void add_fuel_tank(int capacity);
	void list_fuel_tanks();
	void remove_fuel_tank(int tank_id);
	bool open_valve(int tank_id);
	bool close_valve(int tank_id);

	void connect_fuel_tank_to_engine(int tank_id); //tanký ekle

	void disconnect_fuel_tank_from_engine(int tank_id); //tanki ayir

	int print_fuel_tank_count();
	int list_connected_tanks();
	void print_tank_info(int tank_id);
	void fill_tank(int tank_id, double fuel_quantity);

	bool break_fuel_tank(int tank_id);
	bool repair_fuel_tank(int tank_id);
};
