#include <iostream>
#include <fstream>


class Fuel {
public:
	double fuel_quantity = 50;
	double fuel_capacity = 100.0;
	bool fuel_tank = 1; //tank var
	double fuel = 50; // eklenen benzin
	int tank_id;
	double fuel_per_second = 5.5;
	

	double add_fuel(int tank_id, double quantity);
	double absorb_fuel(double fuel_quantity);
	double print_total_fuel_quantity();
	double print_total_consumed_fuel_quantity();
};
