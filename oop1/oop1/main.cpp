#include "Tank.h"
#include "Engine.h"
#include "Fuel.h"
#include "Simulation.h"

double fuel_quantity = 50;
double fuel_capacity = 100.0;
bool fuel_tank = 1; //tank var
double fuel = 50; // eklenen benzin
int tank_id;


int main()
{
	Engine engine;
	engine.start_engine(); //calistir ya da calistirma

	Fuel fuel;
	//fuel.absorb_fuel(fuel_quantity);
	fuel.add_fuel(tank_id, fuel_quantity); 
	fuel.print_total_fuel_quantity();
	fuel.print_total_consumed_fuel_quantity();

	Tank tank;
	tank.add_fuel_tank(fuel_capacity);
	tank.list_fuel_tanks();
	tank.remove_fuel_tank(tank_id);
	tank.connect_fuel_tank_to_engine(tank_id);
	tank.break_fuel_tank(tank_id);
	tank.repair_fuel_tank(tank_id);
	tank.print_fuel_tank_count();
	tank.list_connected_tanks();
	tank.print_tank_info(tank_id);
	tank.fill_tank(tank_id,fuel_quantity);

	Simulation simulation;
	simulation.stop_simulation();

	return 0;
}