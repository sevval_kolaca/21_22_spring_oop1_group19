#include <iostream>
#include <fstream>

using namespace std;

class Engine {
public:
	double fuel_quantity = 50;
	double fuel_capacity = 100.0;
	bool fuel_tank = 1; //tank var

	void start_engine();
	void stop_engine();
};
